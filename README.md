# User Connections 

Uma api focada na criação usuários e endereços, para que os usuários possam
criar conexões com um banco de dados.

# Decrição

Para utilizar a API em desenvolvimento, clone o repositório abaixo:

<https://gitlab.com/hirokz7/user-connections-back-end>

Para utilizar a API em produção, segue link do deploy:

<https://user-connections-back-end.herokuapp.com/>

# Installation

Em desenvolvimento, execute o comando abaixo para installar todas as dependências:

`yarn install`

Neste projeto utilizamos docker para a criação do banco de dados utilizado, execute o comando abaixo
após as instalações das dependências:

`docker-compose up`


# Como utilizar

Com o seu API client é possível utilizar as seguintes rotas:

### Routes

**POST /user**

Rota para a criação de um usuário, onde será encaminhado um e-mail para confirmação de conta no front-end:

RESPONSE STATUS -> HTTP 201(CREATED)

Body:

```json
{
  "username": "carlos",
	"name": "Carlos",
	"cpf": "23112490502", 
	"email": "hiro1kz77@gmail.com",
	"password": "12345678"
}

```

Response:

```json
{
  "username": "carlos",
  "name": "Carlos",
  "cpf": "23112490502",
  "email": "hiro1kz77@gmail.com",
  "activated": false,
  "id": "c24119f6-9f4c-4d46-9556-fd28228a5882",
  "createdAt": "2022-02-21T12:31:03.601Z",
  "updatedAt": "2022-02-21T12:31:03.601Z"
}
```

**POST /activate_account/:username**

Para que se possa realizar o login é necessário fazer a ativação da conta.

RESPONSE STATUS -> HTTP 200(OK)

Body:

_No body_

Response:

```json
{
  "message": "Activated Account!"
}
```

**POST /login**

Para que se possa fazer a requisição nas rotas protegidas é necessário o login com uma conta ativa.

RESPONSE STATUS -> HTTP 200(OK)

Body:

```json
{
    "email": "hiro1kz77@gmail.com",
    "password": "12345678"
}
```

Response:

```json
{
  "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyRGF0YSI6eyJpZCI6ImMyNDExOWY2LTlmNGMtNGQ0Ni05NTU2LWZkMjgyMjhhNTg4MiIsInVzZXJuYW1lIjoiY2FybG9zIiwibmFtZSI6IkNhcmxvcyIsImNwZiI6IjIzMTEyNDkwNTAyIiwiZW1haWwiOiJoaXJvMWt6NzdAZ21haWwuY29tIiwiYWN0aXZhdGVkIjp0cnVlLCJjcmVhdGVkQXQiOiIyMDIyLTAyLTIxVDEyOjMxOjAzLjYwMVoiLCJ1cGRhdGVkQXQiOiIyMDIyLTAyLTIxVDEyOjM0OjM5LjExOFoifSwiaWF0IjoxNjQ1NDQ3NDg5LCJleHAiOjE2NDU1MzM4ODl9.1bZsHpuMokztYxYyuZA7FdlnTwJSeGHtKiG4nRyEPq0"
}
```

**GET /user**

Rota que retorna todos os usuários:


RESPONSE STATUS -> HTTP 200(OK)

Body:

_No body_

Response:

```json
[
  {
    "id": "c24119f6-9f4c-4d46-9556-fd28228a5882",
    "username": "carlos",
    "name": "Carlos Lima",
    "cpf": "23112490502",
    "email": "hiro1kz77@gmail.com",
    "activated": true,
    "createdAt": "2022-02-21T12:31:03.601Z",
    "updatedAt": "2022-02-21T12:47:35.752Z"
  },
  {
    "id": "04fd9eba-2a2c-4a8a-b90d-245b047bad66",
    "username": "karinam",
    "name": "Karina Mori",
    "cpf": "23112490502",
    "email": "karina.mmay@gmail.com",
    "activated": false,
    "createdAt": "2022-02-21T13:14:19.634Z",
    "updatedAt": "2022-02-21T13:14:19.634Z"
  }
]
```

**GET /user/:user_id**

Rota que retorna o próprio usuário logado:

Authorization Header:

**Necessário Bearer Token no header e ser o dono do id**

RESPONSE STATUS -> HTTP 200(Ok)

Body:

_No body_

Response:

```json
{
  "id": "c24119f6-9f4c-4d46-9556-fd28228a5882",
  "username": "carlos",
  "name": "Carlos",
  "cpf": "23112490502",
  "email": "hiro1kz77@gmail.com",
  "activated": true,
  "createdAt": "2022-02-21T12:31:03.601Z",
  "updatedAt": "2022-02-21T12:34:39.118Z",
  "adresses": []
}
```

**PATCH /user/:user_id**

Rota para atualização do seu usuário, é possível atualizar mais de um campo, com todos sendo verificados
para não ir dados incorretos para o DB:

Authorization Header:

**Necessário Bearer Token no header e ser o dono do id**

RESPONSE STATUS -> HTTP 200(Ok)

Body:

````json
{
  "name": "Carlos Lima"
}

Response:

```json
{
  "id": "c24119f6-9f4c-4d46-9556-fd28228a5882",
  "username": "carlos",
  "name": "Carlos Lima",
  "cpf": "23112490502",
  "email": "hiro1kz77@gmail.com",
  "activated": true,
  "createdAt": "2022-02-21T12:31:03.601Z",
  "updatedAt": "2022-02-21T12:47:35.752Z"
}
````


**GET /cep/:cep_number**

Rota para a verificação da existência do CEP, retornando erro caso não exista.

RESPONSE STATUS -> HTTP 200(Ok)

Body:

_No body_

Response:

```json
{
  "cep": "05185-420",
  "logradouro": "Rua Pêra-Marmelo",
  "complemento": "",
  "bairro": "Jardim Santa Lucrécia",
  "localidade": "São Paulo",
  "uf": "SP",
  "ibge": "3550308",
  "gia": "1004",
  "ddd": "11",
  "siafi": "7107"
}
```

RESPONSE ERROR STATUS -> HTTP 404(Not Found)

```json
{
  "message": "CEP not Found!"
}
```

**POST /adress**

Rota para a criação de um endereço para o usuário, onde será também feito a verificação da existência do CEP, sendo necessário apenas o envio do CEP e Número:

**Necessário Bearer Token no header**

RESPONSE STATUS -> HTTP 201(CREATED)

Body:

```json
{
	"cep": "05185420",
	"number": "511"
}

```

Response:

```json
{
  "id": "c24119f6-9f4c-4d46-9556-fd28228a5882",
  "username": "carlos",
  "name": "Carlos Lima",
  "cpf": "23112490502",
  "email": "hiro1kz77@gmail.com",
  "activated": true,
  "createdAt": "2022-02-21T12:31:03.601Z",
  "updatedAt": "2022-02-21T12:47:35.752Z",
  "adresses": [
    {
      "id": "fe31051c-8288-4a9f-a8e1-7cf6a2f8f810",
      "street": "Rua Pêra-Marmelo",
      "number": "511",
      "district": "Jardim Santa Lucrécia",
      "city": "São Paulo",
      "state": "SP",
      "cep": "05185420",
      "createdAt": "2022-02-21T12:49:45.973Z",
      "updatedAt": "2022-02-21T12:49:45.973Z"
    }
  ]
}
```

**GET /adress**

Rota que retorna todos os endereços cadastrados:

RESPONSE STATUS -> HTTP 200(OK)

**Necessário Bearer Token no header**

Body:

_No body_

Response:

```json
[
  {
    "id": "fe31051c-8288-4a9f-a8e1-7cf6a2f8f810",
    "street": "Rua Pêra-Marmelo",
    "number": "511",
    "district": "Jardim Santa Lucrécia",
    "city": "São Paulo",
    "state": "SP",
    "cep": "05185420",
    "createdAt": "2022-02-21T12:49:45.973Z",
    "updatedAt": "2022-02-21T12:49:45.973Z"
  }
]
```

**GET /adress/:user_id**

Rota que retorna todos os endereços de um usuário específico:

**Necessário Bearer Token no header**

RESPONSE STATUS -> HTTP 200(OK))

Body:

_No body_


Response:

```json
[
  {
    "id": "fe31051c-8288-4a9f-a8e1-7cf6a2f8f810",
    "street": "Rua Pêra-Marmelo",
    "number": "511",
    "district": "Jardim Santa Lucrécia",
    "city": "São Paulo",
    "state": "SP",
    "cep": "05185420",
    "createdAt": "2022-02-21T12:49:45.973Z",
    "updatedAt": "2022-02-21T12:49:45.973Z"
  }
]
```

**DELETE /adress/:adress_id**

Rota que deleta o endereço de um usuário:

**Necessário Bearer Token no header e ser o dono do endereço**

RESPONSE STATUS -> HTTP 204(No Content))

Body:

_No body_


Response:

_No body_

## Technologies

- TypeScript
- Express.js
- Nodemailer
- Docker
- Cors
- NodeJs
- PostgreSQL
- Axios
- Bcrypt
- Dotenv
- TypeORM
