import { Request, Response, NextFunction} from "express";

import { createUser, listUser, findId, updateUser } from "../services/user.service";

export const create = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const user = await createUser(req.body, next);

        const { password: user_password, ...userWithoutPassword } = user
        if(user){
            res.status(201).json(userWithoutPassword);
        }
    } catch (error) {
        next(error);
    }

}

export const list = async (req: Request, res: Response, next: NextFunction) => {
    try{
        const users = await listUser();
    
        const listUsers = users.map(item => {
            const { password: user_password, ...userWithoutPassword} = item
            return userWithoutPassword
          })
    
        res.status(200).json(listUsers);
    } catch (error) {
        next(error)
    }
}

export const findUser = async (req: Request, res: Response, next: NextFunction) => {
    try{
        const { id } = req.params
        const user = await findId(id, next)
     
        if(user){
            res.status(200).json(user);
        }
    } catch (error){
        next(error)
    }
}

export const update = async (req: Request, res: Response, next: NextFunction) => {
    try{   
        const { id } = req.params
        const user = await updateUser(req.body, id, next)

        if (user){
            res.status(200).json(user)
        }
    } catch(error){
        next(error)
    }
}
