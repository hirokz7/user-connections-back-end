import { Request, Response, NextFunction} from "express";
import { activateAccount } from "../services/activate.service";


export const activate = async (req: Request, res: Response, next: NextFunction) => {
    try {   
        const { activate_code } = req.params
        const username = activate_code.split('@').splice(0,1).join()

        const activated = await activateAccount(username, next);

        if(activated){
            res.status(200).json(activated);
        }
    } catch (error) {
        next(error);
    }

}