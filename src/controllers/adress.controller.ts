import { Request, Response, NextFunction} from "express";

import { createAdress, listAdress, findUserAdresses, deleteAdress, cepCheck } from "../services/adress.service";

export const create = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const buy = await createAdress(req, next);
        
        if(buy){
            res.status(201).json(buy);
        }
    } catch (error) {      
        next(error);
    }
}


export const list = async (req: Request, res: Response, next: NextFunction) => {
    try{
        const adresses = await listAdress(req);
    
        res.status(200).json(adresses);
    } catch (error) {
        next(error)
    }
}

export const listUserAdress = async (req: Request, res: Response, next: NextFunction) => {
    try{
        const { id } = req.params
        const userAdresses = await findUserAdresses(id, next)
    
        if(userAdresses){
            res.status(200).json(userAdresses);
        }
    } catch (error) {
        next(error)
    }
}

export const listCep = async (req: Request, res: Response, next: NextFunction) => {
    try{ 
        const { cep } = req.params
        const adress = await cepCheck(cep)

        if (adress) {
            res.status(200).json(adress)
        }

    }catch (error){
        next(error)
    }
}

export const destroy = async (req: Request, res: Response, next: NextFunction) => {
    try{
        const userAdresses = await deleteAdress(req, next)
    
        if(userAdresses){
            res.sendStatus(204);
        }
    } catch (error) {
        next(error)
    }
}
