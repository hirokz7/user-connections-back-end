import { NextFunction, Request, Response } from "express";
import { authenticateUser } from "../services/login.service";

export const login = async (req: Request, res: Response, next: NextFunction) => {
    try{
        const { email, password } = req.body;
        const token = await authenticateUser(email, password, next);

        if (token){
            res.status(200).json({ token });
        }
    } catch (error) {
        next(error)
    }
}