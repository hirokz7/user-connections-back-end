import { getRepository, getCustomRepository, RelationId } from "typeorm";
import { Adress, User } from "../entities";
import { Request, NextFunction, Response } from "express";
import AppError from "../errors/AppError";
import AdressRepository from '../repositories/adressRepository';
import axios from "axios";
import UserRepository from "../repositories/userRepository";


export const cepCheck = async (cep: string) => {
    try{
        const cepPath = `https://viacep.com.br/ws/${cep}/json/`
        const response = await axios.get(cepPath)
    
        return response.data
    } catch (error) {
        throw new AppError("CEP not Found!", 404)
    }
}


export const createAdress = async (req: Request, next: NextFunction) => {
    try{

        const { cep, number } = req.body
        
        const adress = await cepCheck(cep)
        
        const adressRepository = getCustomRepository(AdressRepository);
        const userRepository = getRepository(User);
        
        const user = await userRepository.findOne({
            where: {
                id: req.user.userData.id
            }
        })

        if (!user){
            throw new AppError("User not Found!", 404)
        }

        const adressCheck = await adressRepository.findByCepNumber(cep, number, user)

        if(!adressCheck){
            const newAdress = adressRepository.create({
                number: number,
                street: adress.logradouro,
                district: adress.bairro,
                city: adress.localidade,
                state: adress.uf,
                cep: cep,
                user
            });

            await adressRepository.save(newAdress)
        }

        const userAdress = await userRepository.findOne({
            where: {
                id: req.user.userData.id
            },
            relations: ['adresses']
        })

        const {password: user_password, ...userWhithoutPassword }: any = userAdress 

        return userWhithoutPassword
    } catch (error) {
        next(error);
    }
    
}


export const listAdress = async (req: Request) => {
    const adressRepository = getRepository(Adress);

    const adresses = await adressRepository.find({relations: ['user']});

    return adresses;
}

export const findUserAdresses = async (id: string, next: NextFunction) => {
    try{
        const userRepository = getRepository(User);
        
        const user = await userRepository.findOne({
            where: {
                id: id
            },
            relations:['adresses']
        })

        if(!user){
            throw new AppError("User not Found!", 404)
        }

        return user.adresses;
        
    } catch (error) {
        next(error)
    }
}

export const deleteAdress = async (req: Request, next: NextFunction) => {
    try{
        const userRepository = getCustomRepository(UserRepository);
        const adressRepository = getRepository(Adress);

        const user = await userRepository.findOne({
            where: {
                id: req.user.userData.id
            },
            relations: ['adresses']
        });

        const adress = await adressRepository.findOne({
            where:{
                id: req.params.adress_id
            }
        });
 
        if (!adress) {
            throw new AppError("Adress not found, id must be wrong or missing.", 404);             
        } 

        const owner = await user?.adresses.filter((item) => {
            return item.id === adress.id;})
            
        if (owner?.length === 0){
            throw new AppError("Adress not yours, unauthorized to delete.", 400);  
        }
    
        await adressRepository.delete(adress?.id as any)

        return { message: 'Delete Success!'}  
    } catch (error){
        next(error);
    }
}