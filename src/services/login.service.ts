import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
import { getCustomRepository } from "typeorm"
import UserRepository from '../repositories/userRepository'
import AppError from '../errors/AppError';
import { NextFunction } from 'express';

export const authenticateUser = async (email: string, password: string, next: NextFunction) => {
    try{
        const userRepository = getCustomRepository(UserRepository);

        const user = await userRepository.findByEmail(email);

        if (!user || !bcrypt.compareSync(password, user.password)) {
            throw new AppError("Wrong email/password", 401);
        }
        if (!user.activated){
            throw new AppError("Not activated account!", 400);
        }


        const { password: user_password, ...userData } = user;


        const token = jwt.sign({ userData }, process.env.SECRET as string, { expiresIn: '1d' });
        
    
        return token;
    } catch (error){
        next(error)
    }
}