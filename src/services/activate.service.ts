import { getRepository, getCustomRepository } from "typeorm";
import { NextFunction } from "express";
import AppError from "../errors/AppError";
import { User } from "../entities";


export const activateAccount = async (username: string, next: NextFunction) => {
    try{   
        const userRepository = getRepository(User);
        
        const user = await userRepository.findOne({
            where: {
                username: username
            }
        });
       
        if(!user) {
            throw new AppError("User Not Found!", 404)
        }

        user.activated = true
        await userRepository.save(user)
        
        return {message: 'Activated Account!'};
    } catch(error){
        next(error)
    }
}
