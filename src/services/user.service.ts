import { getRepository, getCustomRepository } from "typeorm";
import { NextFunction } from "express";
import { User } from "../entities";
import AppError from "../errors/AppError";
import UserRepository from '../repositories/userRepository';
import bcrypt from 'bcrypt';
import { activateAccountEmail } from './email.service'

interface UserBody {
    username: string;
    name: string;
    cpf: string;
    email: string;
    password: string;
}

export const createUser = async (body: UserBody, next: NextFunction) => {
    try{
        const { username, name, cpf, email, password } = body;
        
        const userRepository = getRepository(User);
        
        const user = userRepository.create({
            username,
            name,
            cpf,
            email,
            password,
        });
        
        await userRepository.save(user);
                        
        await activateAccountEmail(email, next)
        
        return user;
    } catch (error) {
        throw new AppError("E-mail/Username already registered", 400);
    }
    
}

export const listUser = async () => {
    const userRepository = getCustomRepository(UserRepository);

    const users = await userRepository.find();

    return users;
}

export const findId = async (id: string, next: NextFunction) => {
    try{
        const userRepository = getCustomRepository(UserRepository);
    
        const user = await userRepository.findOne({
            where:{
                id: id
            },
            relations: ['adresses']
        });

        if (!user) {
            throw new AppError("User Not Found", 404);
        }
    
        const { password: user_data, ...userWithoutPassword }: any = user

        return userWithoutPassword;
    } catch (error) {
        next(error);
    }
}

export const updateUser = async (body: any, id: string, next: NextFunction) => {
    try{
        const user_data = body;
        const userRepository = getRepository(User);
        const user = await userRepository.findOne(id);
        const allowedKeys = ['username', 'name', 'email','cpf', 'password']

        if(!user){
            throw new AppError("User Not Found!", 404);
        }

        for (let key in body){
            if (!allowedKeys.includes(key)){
                throw new AppError(`The key:'${key}' is Not Allowed!`, 400);
            }
        }
        
        if (user_data.password) {
            const passwordHash = await bcrypt.hash(user_data.password, 10);
            user_data.password = passwordHash;
        }

        user.updatedAt = new Date()

        await userRepository.update(id, user_data);

        const updatedUser = await userRepository.findOne(id);

        const { password: user_password, ... userWithoutPassword }: any = updatedUser

        return userWithoutPassword
    
    } catch (error){
        next(error);
    }
}

