import nodemailer from 'nodemailer'
import path from 'path';
import hbs, {NodemailerExpressHandlebarsOptions} from 'nodemailer-express-handlebars'
import { NextFunction, Request } from 'express'
import { getCustomRepository } from "typeorm"
import UserRepository from '../repositories/userRepository'
import AppError from "../errors/AppError";
import randomstring from 'randomstring'
import dotenv from 'dotenv'

dotenv.config({ path: '.env' });


export const transport = nodemailer.createTransport({
    service: process.env.EMAIL_SERVICE,
    host: process.env.EMAIL_HOSTER,
    auth: {
      user: process.env.EMAIL_USER,
      pass: process.env.EMAIL_PASS
    }
});



const handlebarOption: NodemailerExpressHandlebarsOptions = {
    viewEngine: {
        partialsDir: path.resolve(__dirname, '..', 'templates'),
        defaultLayout: undefined
    },
    viewPath: path.resolve(__dirname, '..', 'templates')
}

transport.use('compile', hbs(handlebarOption));

export const mailTemplateOptions = (to: string[], subject: string, template: string, context: any) => {
    return {
        from: 'no-repply@hiro7end.com.br',
        to,
        subject,
        template,
        context
    };
}

export const activateAccountEmail = async (email: string, next: NextFunction) => {
    try{
   
        const userRepository = getCustomRepository(UserRepository);
    
        const user: any = await userRepository.findByEmail(email);

        if (!user) {
            throw new AppError('User Not Found!', 404);
        }

        const code = await `${user.username}@${randomstring.generate(10)}`

        const options = mailTemplateOptions(
            [email],
            'Activate Account',
            'activate',
            {
                name: user.name,
                code: code
            }
        );

        transport.sendMail(options, function (error, info) {
            if (error) {
                next(error)
            } else {
                console.log(info);
            }
        });

        return { message: 'Email sent' }        
    } catch (error){
        throw new AppError('User Not Found!', 404);
    }
}

export const emailToUser = async (req: Request, next: NextFunction) => {
    try{
        const { email, message } = req.body

        const userRepository = getCustomRepository(UserRepository);
    
        const user: any = await userRepository.findByEmail(email);

        if (!user) {
            throw new AppError('User Not Found!', 404);
        }

        const options = mailTemplateOptions(
            [email],
            'Activate Account',
            'email',
            {
                name: user.name,
                message: message
            }
        );

        transport.sendMail(options, function (error, info) {
            if (error) {
                next(error)
            } else {
                console.log(info);
            }
        });

        return { message: 'Email sent' }        
    } catch (error){
        throw new AppError('User Not Found!', 404);
    }
}