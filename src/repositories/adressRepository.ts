import { EntityRepository, Repository } from "typeorm";
import { Adress } from "../entities/Adress";

@EntityRepository(Adress)
class adressRepository extends Repository<Adress> {
    public async findByCepNumber(cep: string, number:string, user:any): Promise<Adress | undefined> {
        const adress = await this.findOne({
            where: {
                cep,
                number,
                user
            }
        });

        return adress;
    };
    public async findById(id: string): Promise<Adress | undefined> {
        const adress = await this.findOne({
            where: {
                id
            }
        });

        return adress;
    }
}

export default adressRepository ;
