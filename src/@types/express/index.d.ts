declare namespace Express {
    interface Request {
        user: { 
            userData: { 
                id: string, 
                username: string,
                name: string,
                cpf: string,
                email: string,
                password: string,
                activated: boolean,
                createdAt: string,
                updatedAt: string,
                adresses: Array[]
            },
   
        }
    }
}
