import { Express } from "express";
import { userRouter } from "./user.router"
import { loginRouter } from "./login.router";
import { activateRouter } from "./activate.router";
import { adressRouter } from "./adress.router"
import { cepRouter } from "./cep.router";
import cors from 'cors'


export const initializerRouter = (app: Express) => {
    app.use(cors())
    app.use('/user', userRouter());
    app.use('/login', loginRouter());
    app.use('/cep', cepRouter())
    app.use('/adress', adressRouter());
    app.use('/activate_account', activateRouter());
}