import { Router } from "express";

import { create, list, findUser, update} from "../controllers/user.controller";
import { owner } from "../middlewares/owner.middleware";
import { isAuthenticated } from '../middlewares/authentication.middleware'

const router = Router();

export const userRouter = () => {
    router.post('', create);
    router.get('', list);
    router.get('/:id', isAuthenticated, owner, findUser);
    router.patch('/:id', isAuthenticated, owner, update);
    return router;
}