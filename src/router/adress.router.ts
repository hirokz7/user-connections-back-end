import { Router } from "express";
import { isAuthenticated } from "../middlewares/authentication.middleware";
import { create, list, listUserAdress, destroy} from '../controllers/adress.controller'

const router = Router();

export const adressRouter = () => {
    router.post('', isAuthenticated, create);
    router.get('', isAuthenticated, list);
    router.get('/:id', isAuthenticated, listUserAdress);
    router.delete('/:adress_id', isAuthenticated, destroy);
    
    return router;
}