import { Router } from "express";
import { isAuthenticated } from "../middlewares/authentication.middleware";
import { listCep } from '../controllers/adress.controller'

const router = Router();

export const cepRouter = () => {
    router.get('/:cep', listCep);

    return router;
}