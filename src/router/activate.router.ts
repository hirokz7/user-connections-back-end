import { Router } from "express";
import { activate } from '../controllers/activate.controller'

const router = Router();

export const activateRouter = () => {
    router.post('/:activate_code', activate);
    
    return router;
}