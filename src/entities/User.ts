import { 
    Entity, 
    Column, 
    CreateDateColumn, 
    PrimaryGeneratedColumn, 
    UpdateDateColumn, 
    BeforeInsert,
    OneToMany } from "typeorm";
import bcrypt from "bcrypt";
import { Adress } from "./Adress";
  
@Entity("users")
export class User {
    @PrimaryGeneratedColumn("uuid")
    id!: string;

    @Column({ unique: true, nullable: false })
    username!: string;

    @Column({ nullable: false })
    name!: string;

    @Column({ nullable: false })
    cpf!: string;

    @Column({ unique: true, nullable: false })
    email!: string;

    @Column({ nullable: false })
    password!: string;

    @Column()
    activated!: boolean;

    @CreateDateColumn()
    createdAt!: Date;

    @UpdateDateColumn()
    updatedAt!: Date;

    @BeforeInsert()
    async hashPassword() {
        this.password = await bcrypt.hash(this.password, 10);
        this.activated = false
    }

    @OneToMany(() => Adress, adress => adress.user)
    adresses!: Adress[];
}
  