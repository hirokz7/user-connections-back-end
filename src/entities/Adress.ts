import { 
    Entity, 
    Column, 
    CreateDateColumn, 
    PrimaryGeneratedColumn,
    ManyToOne, 
    UpdateDateColumn} from "typeorm";
import { User } from ".";

  
@Entity("adresses")
export class Adress {
    @PrimaryGeneratedColumn("uuid")
    id!: string;

    @Column()
    street!: string;

    @Column()
    number!: string;

    @Column()
    district!: string;

    @Column()
    city!: string;

    @Column()
    state!: string;

    @Column({ nullable: false })
    cep!: string;

    @CreateDateColumn()
    createdAt!: Date;

    @UpdateDateColumn()
    updatedAt!: Date;
    
    @ManyToOne(type => User, user => user.adresses) 
    user!: User; 
}
  
