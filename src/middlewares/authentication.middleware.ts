import jwt from 'jsonwebtoken';
import { Request, Response, NextFunction } from 'express';
import AppError from '../errors/AppError';

export const isAuthenticated = (req: Request, res: Response, next: NextFunction) => {
   try{
    const token = req.headers.authorization?.split(' ')[1];

    jwt.verify(token as string, process.env.SECRET as string, (err: any, decoded: any) => {
        if (err) {
            throw new AppError("Missing authorization headers", 401);
        }

        req.user = { userData: decoded.userData };

        next();
    });
   } catch (error){
        next(error);
   }
}